
(function( $ ) {
    $(window).load(function(){

        //min-height adjustment for page
        var winHt=$(window).outerHeight();
        var headerHt=$('.js-header').outerHeight();
        var footerHt=$('.js-footer').outerHeight();
        var mh=winHt-headerHt-footerHt;
        $('.main-contents').css("min-height", mh);

        // slider buttons
        $('#myCarousel .item:first-child').addClass('active');
    var totalDot = $('#myCarousel .item').length;
    var dots = '';
    var i;
    for (i = 0; i < totalDot; i++) {
      dots += '<li data-target="#myCarousel" data-slide-to="'+i+'"></li>';
    }
    //console.log(dots);
    $('<ol class="carousel-indicators">'+dots+'</ol>').prependTo('#myCarousel');
    $('.carousel-indicators li:first-child').addClass('active');
    $("#myCarousel").on('click', '.carousel-indicators li', function(e){
      $(this).addClass('active').siblings().removeClass('active');
      e.preventDefault();
    });
     $('#myCarousel').on('slid.bs.carousel', function () {
      //alert("Slide Event");
      var currentIndexPop = $('#myCarousel .item.active').index();
      $('#myCarousel .carousel-indicators li').eq(currentIndexPop).addClass('active').siblings().removeClass('active');
  });
  var containerHt=$('.sidebar').height();
  $('.single-contents').css('min-height', containerHt);
    });




    $(window).resize(function(){

        //min-height adjustment for page
        var winHt=$(window).height();
        var headerHt=$('.js-header').outerHeight();
        var footerHt=$('.js-footer').outerHeight();
        var mh=winHt-headerHt-footerHt;
        $('.main-contents').css("min-height", mh);
        var containerHt=$('.sidebar').height();
        $('.single-contents').css('min-height', containerHt);
        var hdHt=$('.ik-video-slider-container .slider-panel1 h2').height();
        $('.ik-video-single .ik-social-icons').css('min-height', hdHt);
    });

    //menu
    $('.lines-button').on('click',function(){

        $(this).toggleClass('close');
        $('html, body').toggleClass('menu-active');
        //$('.main-menu-container').slideToggle();
        if ($('.lines-button').hasClass('close')){
            // //min-height adjustment for page
            // var winHt=$(window).outerHeight();
            // var headerHt=$('.js-header').outerHeight();
            // var mh=winHt-headerHt;
            // $('.main-contents').css("height", mh);
        }else{
            //$('.main-contents').css("height", "auto");
        }


    });
    $('.menu-close-layer, .nav-container ul li  a').on('click',function(){

        $('.lines-button').removeClass('close');
        $('html, body').removeClass('menu-active');
        $('.main-contents').css("height", "auto");

    });

    if($('.news_scroll').length > 0){
        $('.news_scroll').simplyScroll({
            orientation: 'vertical',
            manualMode: 'loop',
        });
    }
    //animations for sections and elements
    var $window = $(window),
    win_height_padded = $window.height() * 1.1,
    isTouch = Modernizr.touch;

    if (isTouch) {
        $('.revealOnScroll').addClass('animated');
    }

    $window.on('scroll', revealOnScroll);

    function revealOnScroll() {
        var scrolled = $window.scrollTop(),
        win_height_padded = $window.height() * 1.6;

        // Showed...
        $(".revealOnScroll:not(.animated)").each(function() {
            var $this = $(this),
            offsetTop = $this.offset().top;

            if (scrolled + win_height_padded > offsetTop) {
                if ($this.data('timeout')) {
                    window.setTimeout(function() {
                        $this.addClass('animated ' + $this.data('animation'));
                    }, parseInt($this.data('timeout'), 10));
                } else {
                    $this.addClass('animated ' + $this.data('animation'));
                }
            }
        });
        // Hidden...
        $(".revealOnScroll.animated").each(function(index) {
            var $this = $(this),
            offsetTop = $this.offset().top;
            if (scrolled + win_height_padded < offsetTop) {
                $(this).removeClass('animated fadeInUp flipInX lightSpeedIn');
            }
        });
    }
    revealOnScroll();


    var containerHt=$('.sidebar').height();
    $('.single-contents').css('min-height', containerHt);

    //releases show more button
    $('.show-more-btn').on('click',function(){
        $('.releases-nav li.hide-me').slideToggle();
    });

    // $(".red-box .post-wrapper").niceScroll({
    //     cursorcolor: "#fff",
    //     cursorwidth: "10px",
    //     cursorborderradius: "5px",
    //     autohidemode: true,
    // });

var $owl = $('.owl-carousel');
// $('#accordion').on('shown.bs.collapse', function () {
//     var evt = document.createEvent('UIEvents');
//     evt.initUIEvent('resize', true, false,window,0);
//     window.dispatchEvent(evt);
//    });
$owl.on('initialized.owl.carousel resized.owl.carousel', function(e) {
    $(e.target).toggleClass('hide-nav', e.item.count <= e.page.size);
});
    $('.owl-carousel').owlCarousel({
    loop:false,
    margin:0,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        767:{
            items:2,
            nav:true
        },
        1200:{
            items:4,
            nav:true,
            loop:false
        }

    }
});
var owl = $('.owl-carousel');
  owl.owlCarousel();

  owl.on('changed.owl.carousel', function(event) {

    if( event.item.index === 0) {
      $('.owl-prev').addClass('inactive');
    }
    else if( event.item.index == event.item.count- 1) {
      $('.owl-next').addClass('inactive');
    }
    else {
       $('.owl-prev').removeClass('inactive');
       $('.owl-next').removeClass('inactive');
    }
 });
var owl_new = $('.owl-carousel2');
    owl_new.owlCarousel({
    loop:false,
    margin:0,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:false,
            dots:false
        },
        481:{
            items:4,
            nav:true,
            loop:false
        }
    }

});

$('.owl-carousel2 .owl-item:first-child').addClass('circle-active');
$('.owl-carousel2 .owl-item a').on('click', function(){
    $(this).parent().parent().siblings().removeClass('circle-active');
    $(this).parent().parent().addClass('circle-active');
});

    $('.owl-carousel4').on('initialized.owl.carousel resized.owl.carousel', function(e) {
        $(e.target).toggleClass('hide-nav', e.item.count <= e.page.size);
    });
    $('.owl-carousel4').owlCarousel({
        loop:false,
        margin:0,
        responsiveClass:true,
        nav:true,
        dots : false,
        responsive:{
            0:{
                items:1,
            },
            767:{
                items:2,
            },
            1200:{
                items:3,
            }

        }
    });
    var owl4 = $('.owl-carousel4');
      owl4.owlCarousel();

      owl4.on('changed.owl.carousel', function(event) {

        if( event.item.index === 0) {
          $('.owl-carousel4 .owl-prev').addClass('inactive');
        }
        else if( event.item.index == event.item.count- 1) {
          $('.owl-carousel4 .owl-next').addClass('inactive');
        }
        else {
           $('.owl-carousel4 .owl-prev').removeClass('inactive');
           $('.owl-carousel4 .owl-next').removeClass('inactive');
        }
     });
    // if($(window).width() > 1199){
    //     console.log($('.owl-carousel4 .owl-item').length);
    //     if($('.owl-carousel4 .owl-item').length <= 3){
    //         $('.owl-carousel4 .owl-controls').hide();
    //     } else {
    //         $('.owl-carousel4 .owl-controls').show();
    //         $('.owl-carousel4').next().find('.btn').addClass('custom');
    //     }
    // }

$('.ai-faq-tab-trigger').on('click', function(e){
  e.preventDefault();
  if(!$(this).hasClass('active')){
    $(this).addClass('active').closest('.ai-faq-tab-wrapper').siblings().find('.ai-faq-tab-trigger').removeClass('active');
    $(this).next().slideDown().closest('.ai-faq-tab-wrapper').siblings().find('.ai-faq-tab').slideUp();
  }
  else{
    $(this).removeClass('active');
    $(this).next().slideUp();
  }
});

$('.ai-event-section .read_more_posts_btn').on('click', function (e) {
  e.preventDefault();
  $('.ai-calendar-overlay').addClass('in');
  $('body').addClass('modal-open');
});
$('.ai-close-btn').on('click', function (e) {
  e.preventDefault();
  $('.ai-calendar-overlay').removeClass('in');
  $('body').removeClass('modal-open');

});
$('.calendar-slider').owlCarousel({
    loop:false,
    margin:0,
    responsiveClass:true,
    nav:true,
    dots : false,
    responsive:{
        0:{
            items:1,
        },
        767:{
            items:1,
        },
        1200:{
            items:2,
        }

    }
});

// ik js
if($('.ik-slct-container select').length>0){
    $('.ik-slct-container select').dropkick({
      mobile: true
    });
}

var totalItems = $('#ik-photo-slider .item').length;
var currentIndex = $('#ik-photo-slider div.active').index() + 1;
$('.ik-slide-numbers').html(''+currentIndex+'/'+totalItems+'');
$('#ik-photo-slider').bind('slid.bs.carousel', function() {
    currentIndex = $('div.active').index() + 1;
    $('.ik-slide-numbers').html(''+currentIndex+'/'+totalItems+'');
});

$(".ik-photo-single .carousel-control.right, .ik-photo-single .carousel-control.left").on('click', function(){
    $('#ik-photo-slider').attr("data-ride", "slide").addClass('slide').carousel({
        interval:6000,
        pause: ture
    });
});
$(".ik-slide-show-btn").click(function () {


    if(!$('#ik-photo-slider').hasClass('slide')){
        $('#ik-photo-slider').attr("data-ride", "slide").addClass('slide').carousel({
           interval:6000,
           pause: true
        });

    }else{
        $('#ik-photo-slider').attr("data-ride", "").removeClass('slide').carousel({
           interval:false,
           pause: true,
           autoplay:false
        });
        $('#ik-photo-slider').carousel('pause');
    }
    var text = $(this).text();
    $(this).text(
    text == "Lancer le Diaporama" ? "Arrêtez le Diaporama" : "Lancer le Diaporama");



});

$(window).load(function(){
    $('.construction-wrapper').addClass('move-imgs');
    var hdHt=$('.ik-video-slider-container .slider-panel1 h2').height();
    $('.ik-video-single .ik-social-icons').css('min-height', hdHt);
});


$('.main-menu-container .container > ul > li').has('ul').addClass('has-nav');
$('.has-nav').addClass('tab-click');
var windWidth = $(window).width();
if(windWidth < 1025){
    $('.main-menu-container .container > ul > li > a').on('click',function(x){
        if($(this).parent().hasClass('tab-click')){
            //x.preventDefault();
            $(this).next('ul').addClass('nav-active');
            $(this).parent('li').removeClass('tab-click');
        }else{
            //x.preventDefault();
            $(this).next('ul').removeClass('nav-active');
            $(this).parent('li').addClass('tab-click');
        }
    });
}
if($('.posts-pagination .first').length === 0){
    $('.posts-pagination .previous').addClass('take-left');
}
if($('.posts-pagination .last').length === 0){
    $('.posts-pagination .next').addClass('take-right');
}
if($('.posts-pagination .first').length === 0 && $('.posts-pagination .previous').length === 0){
    $('.posts-pagination').addClass('numbers-align-left');
}
if($('.posts-pagination .last').length === 0 && $('.posts-pagination .next').length === 0){
    $('.posts-pagination').addClass('numbers-align-right');
}
})(jQuery);
$(document).ready(function() {
    var equalize = function () {
        var disableOnMaxWidth = 0; // 767 for bootstrap

        var grouped = {};
        var elements = $('*[data-same-height]');

        elements.each(function () {
            var el = $(this);
            var id = el.attr('data-same-height');

            if (!grouped[id]) {
                grouped[id] = [];
            }

            grouped[id].push(el);
        });

        $.each(grouped, function (key) {
            var elements = $('*[data-same-height="' + key + '"]');

            elements.css('height', '');

            var winWidth = $(window).width();

            if (winWidth <= disableOnMaxWidth) {
                return;
            }

            var maxHeight = 0;

            elements.each(function () {
                var eleq = $(this);
                maxHeight = Math.max(eleq.outerHeight(), maxHeight);
            });

            elements.css('height', maxHeight + "px");
        });
    };

    var timeout = null;

    $(window).resize(function () {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }

        timeout = setTimeout(equalize, 250);
    });
    equalize();
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return results[1] || 0;
        }
    }
    $(document).ready(function () {
        if($('.ai-event-hd').length > 0){
            $('.ai-event-hd').addClass('hd-' + $.urlParam('categorie')); 
        }
    });
});
